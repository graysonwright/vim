Grayson Wright's Default Vim Configuration
==========================================

Installation:

    git clone git@bitbucket.org:graysonwright/vim.git

Create symlink(s):

    ln -s ~/.vim/vimrc ~/.vimrc

