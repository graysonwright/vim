" http://nvie.com/posts/how-i-boosted-my-vim/
" 
" https://github.com/mitechie/pyvim
" 
" 


set nocompatible


let mapleader=","	" change the leader to be a comma vs backslash

" Quickly edit/reload the vimrc file
nmap <silent> <leader>ev :e $MYVIMRC<CR>
nmap <silent> <leader>sv :so $MYVIMRC<CR>

set hidden		"hide buffers with unsaved changes

set nowrap		"don't wrap lines
set tabstop=8		"tab is four spaces
set shiftwidth=8
set shiftround		"use multiple of shiftwidth when indenting with '<' and '>'
"set expandtab
set backspace=indent,eol,start	"allow backspacing over everything in insert mode.
set autoindent		"always set autoindenting on
set copyindent		"copy the previous indentation on autoindenting
set number		"show line number
set showmatch		"show matching parenthesis
set ignorecase		"ignore case when searching
set smartcase		"ignore case if search pattern is all lowercase, case-sensitive otherwise.
set smarttab		"insert tabs on the start of a line according to shiftwidth, not tabstop
set hlsearch		"highlight search terms
set incsearch		"show matches as you type

"makes ';' behave like :
nnoremap ; :

cmap w!! w !sudo tee % >/dev/null	"allow saving of files that need sudo

syntax enable

"Enable pasting of large amounts of text.
set pastetoggle=<F2>

"Clear old searches
nmap <silent> ,/ :nohlsearch<CR> 

filetype plugin indent on	"better indenting based on filetype

" Set color scheme
if &t_Co >= 256 || has("gui_running")
	"colorscheme mustang
	colorscheme smyck
endif
if &t_Co > 2 || has("gui_running")
	" switch syntax highlighting on, when the terminal has colors
	syntax on
endif

"plugin settings
filetype plugin on
set nocp " set nocompatible

"folding settings
set foldmethod=indent
set foldnestmax=10
set nofoldenable



" ==============================================================================
" Plugins
" ==============================================================================


" lusty-juggler
" http://www.vim.org/scripts/script.php?script_id=2050
nmap <silent> <Leader>b :LustyJuggler<CR>
" Suppress warnings about ruby
let g:LustyJugglerSuppressRubyWarning = 1

" Set make to automatically use gcc or g++ with C or C++ files
au FileType c set makeprg=gcc\ %
au FileType cpp set makeprg=g++\ %

